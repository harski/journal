extern crate journal;

use journal::settings::Settings;
use std::fs::remove_dir;
use std::path::{Path, PathBuf};

#[test]
fn test_default_settings_initialization() {
    let set = Settings {
        data_dir: PathBuf::from("./tests/data"),
    };

    match set.initialize() {
        Ok(_) => assert!(true),
        Err(e) => assert!(false, e.to_string()),
    };

    let path = Path::new(&set.data_dir);
    // initialization creates path, it should exist
    assert_eq!(path.exists(), true);

    match remove_dir(&path) {
        Ok(_) => assert!(true),
        Err(e) => assert!(false, e.to_string()),
    };

    // path should be deleted now
    assert_eq!(path.exists(), false);
}
