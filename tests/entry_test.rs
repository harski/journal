extern crate journal;
extern crate tempfile;

use journal::entry::Entry;
use journal::settings::Settings;
use journal::tmp_file::TmpFile;
use std::convert::TryFrom;
use std::fs::remove_file;
use std::path::PathBuf;
use tempfile::tempdir;

/// Test if loading an Entry from TmpFile properly resets the Entry path.
#[test]
fn test_edit_entry_path() {
    let tmp_dir = tempdir().unwrap();
    let opt = Settings {
        data_dir: PathBuf::from(tmp_dir.path()),
    };

    let mut entry = Entry {
        heading: "Edit test".to_string(),
        body: Some("Who cares?".to_string()),
        ..Default::default()
    };

    assert!(entry.save(&opt).is_ok());

    let tmp_file = TmpFile {
        path: entry.path.clone().unwrap(),
    };
    let read_entry = Entry::try_from(&tmp_file).unwrap();
    assert_eq!(None, read_entry.path);
    let _ = entry.delete();
}

#[test]
fn test_entry_default() {
    assert_eq!(
        Entry {
            body: None,
            heading: "".to_string(),
            ..Default::default()
        },
        Entry::default()
    );
}

#[test]
fn test_entry_remove() {
    let tmp_dir = tempdir().unwrap();
    let opt = Settings {
        data_dir: PathBuf::from(tmp_dir.path()),
    };

    let mut entry = Entry {
        heading: "Remove test".to_string(),
        body: Some("Who cares?".to_string()),
        ..Default::default()
    };

    assert_eq!((), entry.save(&opt).unwrap());
    assert!(entry.path.clone().unwrap().exists());
    assert_eq!((), entry.delete().unwrap());
}

#[test]
fn test_entry_save() {
    let tmp_dir = tempdir().unwrap();
    let opt = Settings {
        data_dir: PathBuf::from(tmp_dir.path()),
    };

    let mut entry = Entry {
        heading: "Save test".to_string(),
        body: Some("Who cares?".to_string()),
        ..Default::default()
    };

    assert!(entry.save(&opt).is_ok());

    // check that file exists
    assert!(entry.path.clone().unwrap().exists());

    // cleanup
    let _ = remove_file(entry.path.clone().unwrap());
}
