use crate::error::{Error, Result};
use crate::path_utils::is_tmp_file_path;
use crate::settings::Settings;
use crate::tmp_file::TmpFile;
use chrono::{Local, NaiveDate};
use crypto::digest::Digest;
use crypto::sha2::Sha256;
use std::convert::TryFrom;
use std::fmt;
use std::fs::{read_dir, read_to_string, remove_file, File};
use std::io::prelude::*;
use std::path::{Path, PathBuf};

const FILE_SUFFIX: &str = ".md";
const FILE_MAX_INT_PART: usize = 1000;

#[derive(Debug, PartialEq)]
pub struct Entry {
    pub date: NaiveDate,
    pub heading: String,
    pub body: Option<String>,
    pub path: Option<PathBuf>,
}

impl Entry {
    /// Delete Entry.
    pub fn delete(self) -> Result<()> {
        let path = match self.path {
            Some(p) => p,
            None => return Err(Error::PathDoesNotExist),
        };

        remove_file(path)?;
        Ok(())
    }

    /// Generates a new, unique file name for entry.
    fn gen_new_file_path(&self, root: &Path) -> Result<PathBuf> {
        if self.heading.is_empty() {
            return Err(Error::InvalidHeading);
        }

        // generate the entry base name
        let mut base_name = "".to_string();
        for c in self.heading.chars() {
            if c.is_ascii() && !c.is_ascii_whitespace() {
                base_name.push(c);
            } else {
                base_name.push('_');
            }
        }

        for i in 0..FILE_MAX_INT_PART {
            let mut path = root.to_path_buf();
            let mut tmp = base_name.clone();
            if i != 0 {
                tmp.push_str("_");
                tmp.push_str(&i.to_string());
            }
            tmp.push_str(FILE_SUFFIX);
            path.push(tmp);

            if !path.exists() {
                return Ok(path);
            }
        }

        Err(Error::PathGeneration)
    }

    /// Load Entry from path.
    pub fn load(path: &Path) -> Result<Entry> {
        let mut entry = Entry::default();
        entry.path = Some(path.to_path_buf());
        // read to string
        let contents = read_to_string(path)?;
        // parse
        let mut parts = contents.splitn(2, "\n\n");
        if let Some(headers) = parts.next() {
            for line in headers.split('\n') {
                if line.starts_with("Heading:") {
                    let start = "Heading:".len();
                    entry.heading = line[start..].trim().to_string();
                } else if line.starts_with("Date:") {
                    let start = "Date:".len();
                    entry.date =
                        NaiveDate::parse_from_str(line[start..].to_string().trim(), "%Y-%m-%d")?;
                }
                // TODO: Handle unknown header
            }
        };

        if let Some(s) = parts.next() {
            entry.body = Some(s.to_string());
        };
        Ok(entry)
    }

    /// Write Entry to disk
    pub fn save(&mut self, opt: &Settings) -> Result<()> {
        if self.path.is_none() {
            self.path = Some(self.gen_new_file_path(&opt.data_dir)?);
        }
        let content = self.to_string();
        let mut file = File::create(self.path.clone().unwrap())?;
        file.write_all(content.as_bytes())?;
        Ok(())
    }

    /// Set Entry body
    pub fn set_body(&mut self, body: &str) -> Result<()> {
        self.body = Some(body.to_string());
        Ok(())
    }

    /// Get Entry sha256 hash.
    pub fn sha256(&self) -> String {
        let mut s = self.date.to_string();
        s.push_str(&self.heading);
        if let Some(body) = &self.body {
            s.push_str(&body);
        };

        let mut sha = Sha256::new();
        sha.input_str(&s);
        sha.result_str()
    }

    /// Check if hash has the prefix.
    ///
    /// Empty prefix does not match anyting.
    pub fn sha256_prefix_matches(&self, prefix: &str) -> bool {
        if prefix.is_empty() {
            return false;
        }

        let mut res = true;

        let hash = self.sha256();
        let mut cs2 = hash.chars();
        for c1 in prefix.chars() {
            match cs2.next() {
                Some(c) => {
                    if c1 != c {
                        res = false;
                        break;
                    }
                }
                None => {
                    res = false;
                    break;
                }
            };
        }
        res
    }
}

impl Default for Entry {
    fn default() -> Self {
        Self {
            body: None,
            date: Local::now().naive_local().date(),
            heading: "".to_string(),
            path: None,
        }
    }
}

impl TryFrom<&TmpFile> for Entry {
    type Error = Error;

    fn try_from(tmp_file: &TmpFile) -> Result<Entry> {
        let mut entry = Entry::load(&tmp_file.path)?;
        // Don't retain the tmp file path
        entry.path = None;
        Ok(entry)
    }
}

impl fmt::Display for Entry {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut string = String::new();
        string.push_str("Heading: ");
        string.push_str(&self.heading);
        string.push_str("\n");

        string.push_str("Date: ");
        string.push_str(&self.date.to_string());
        string.push_str("\n");

        string.push_str("\n");
        if let Some(b) = &self.body {
            string.push_str(&b);
        }

        write!(f, "{}", string)
    }
}

/// Get all valid entries in the dir 'path'.
pub fn get_entries_from_dir(path: &Path) -> Result<Vec<Entry>> {
    let mut entries: Vec<Entry> = Vec::new();
    let dir_entries = read_dir(path)?;

    for path in dir_entries {
        let path = match path {
            Ok(p) => p,
            Err(e) => {
                eprintln!("Failed to read file: {}", e);
                continue;
            }
        };

        if is_tmp_file_path(&path.path()) {
            // TODO: possibly unsafe unwrap(): path() may not be utf-8
            eprintln!(
                "Found a dangling(?) temporary file {}",
                path.path().to_str().unwrap()
            );
            continue;
        }

        // check if file is a tmp file
        match Entry::load(&path.path()) {
            Ok(e) => {
                entries.push(e);
            }
            Err(e) => {
                eprintln!("Failed to load entry: {}", e);
            }
        };
    }

    Ok(entries)
}

#[cfg(test)]
mod tests {
    use super::*;
    use tempfile::tempdir;

    #[test]
    fn test_entry_file_name_generation_valid() {
        let tmp_dir = tempdir().unwrap();
        let opt = Settings {
            data_dir: PathBuf::from(tmp_dir.path()),
        };

        let entry = Entry {
            heading: "Test heading öä".to_string(),
            body: Some("Who cares?".to_string()),
            ..Default::default()
        };

        let mut assert_dir = opt.data_dir.clone();
        assert_dir.push("Test_heading___.md");
        match entry.gen_new_file_path(&opt.data_dir) {
            Ok(generated) => assert_eq!(PathBuf::from(&assert_dir), generated),
            Err(e) => assert!(false, e.to_string()),
        };
    }

    #[test]
    fn test_sha256_prefix_matches() {
        // hash: "55254db17ca245d891f4bf792221850dfc526a94991bbfbd6e9ca8fb712552b3"
        let entry = Entry {
            date: NaiveDate::from_ymd(2019, 11, 18),
            heading: "Test heading".to_string(),
            body: Some("Test body".to_string()),
            path: None,
        };
        assert!(entry.sha256_prefix_matches("55254db"));
    }

    #[test]
    fn test_sha256_prefix_matches_empty() {
        let entry = Entry::default();
        assert!(!entry.sha256_prefix_matches(""));
    }

    #[test]
    fn test_sha256_prefix_matches_not() {
        let entry = Entry::default();
        assert!(!entry.sha256_prefix_matches("abc"));
    }
}
