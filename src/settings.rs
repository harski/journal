use crate::path_utils;
use std::fs::create_dir_all;
use std::io::Error;
use std::path::PathBuf;

#[derive(Debug, PartialEq)]
pub struct Settings {
    pub data_dir: PathBuf,
}

impl Settings {
    /// Get and initialize default settings
    pub fn default() -> Result<Settings, Error> {
        let settings = Settings {
            data_dir: path_utils::get_data_dir()?,
        };

        match settings.initialize() {
            Ok(()) => Ok(settings),
            Err(e) => Err(e),
        }
    }

    /// Initialize Settings.
    ///
    /// Creates data_dir if necessary.
    pub fn initialize(&self) -> Result<(), Error> {
        // Create data dir
        match create_dir_all(&self.data_dir) {
            Ok(()) => Ok(()),
            Err(e) => Err(e),
        }
    }
}
