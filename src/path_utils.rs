use crate::tmp_file;
use dirs::data_dir;
use std::io::{Error, ErrorKind, Result};
use std::path::{Path, PathBuf};

const DATA_DIR_NAME: &str = "journal";

/// Get user journal data directory path.
///
/// ```
/// # use journal::path_utils;
/// path_utils::get_data_dir();
/// ```
pub fn get_data_dir() -> Result<PathBuf> {
    let mut data_dir_path = match data_dir() {
        Some(path) => path,
        None => {
            return Err(Error::new(
                ErrorKind::Other,
                "Could not resolve user data directory.",
            ));
        }
    };

    data_dir_path.push(DATA_DIR_NAME);
    Ok(data_dir_path)
}

/// Get tmp file dir
///
/// At the moment the actual data dir is used for tmp files as well.
pub fn get_tmp_dir() -> Result<PathBuf> {
    get_data_dir()
}

/// Check if path is to a tmp file.
pub fn is_tmp_file_path(path: &Path) -> bool {
    let pattern = tmp_file::TMP_FILE_BASE;

    // get file name from path
    // TODO: possibly unsafe unwrap(), if path is not utf-8. file_name() returns OsStr.
    let file_name = path.file_name().and_then(|s| s.to_str()).unwrap();

    prefix_match(pattern, file_name)
}

/// Check if prefix is a... prefix of the string.
fn prefix_match(prefix: &str, string: &str) -> bool {
    let mut ps = prefix.chars();
    let mut ss = string.chars();
    let mut res = false;

    loop {
        match (ps.next(), ss.next()) {
            (Some(p), Some(s)) => {
                if p != s {
                    // prefix mismatch
                    break;
                }
            }
            (None, Some(_)) => {
                // prefix matched all the way, is MATCH
                res = true;
                break;
            }
            (None, None) => {
                //prefix == string, is MATCH
                res = true;
                break;
            }
            (Some(_), None) => {
                // prefix is longer than string: prefix mismatch
                break;
            }
        }
    }

    res
}

#[cfg(test)]
mod tests {
    use super::prefix_match;

    #[test]
    fn prefix_match_fail() {
        assert!(!prefix_match("abcdxyz", "abcdefghijk"));
    }

    #[test]
    fn prefix_match_same_strings() {
        assert!(prefix_match("abcd", "abcd"));
    }

    #[test]
    fn prefix_match_short_string() {
        assert!(!prefix_match("abcdefghijk", "abc"));
    }

    #[test]
    fn prefix_match_success() {
        assert!(prefix_match("abcd", "abcdefghijk"));
    }
}
