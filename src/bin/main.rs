extern crate clap;
extern crate journal;

use clap::{App, Arg, ArgMatches, SubCommand};
use journal::entry::{get_entries_from_dir, Entry};
use journal::error::{Error, Result};
use journal::hash_utils::minimal_hash_prefix_length;
use journal::settings::Settings;
use journal::tmp_file::TmpFile;
use std::convert::TryFrom;
use std::env;
use std::path::Path;
use std::process::{exit, Command};

const BIN_NAME: &str = env!("CARGO_PKG_NAME");
const VERSION: &str = env!("CARGO_PKG_VERSION");
const DEFAULT_EDITOR: &str = "vim";
const DEFAULT_PAGER: &str = "less";
const LICENSE: &str = "Licensed under the 2-clause BSD licence.";

fn main() {
    let args = App::new(BIN_NAME)
        .subcommand(SubCommand::with_name("version").about("Show package version"))
        .subcommand(
            SubCommand::with_name("add")
                .about("Add new journal entry")
                .arg(
                    Arg::with_name("date")
                        .short("d")
                        .long("date")
                        .value_name("ENTRY_DATE")
                        .help("Set the entry date.")
                        .takes_value(true)
                        .required(false),
                )
                .arg(
                    Arg::with_name("heading")
                        .short("h")
                        .long("heading")
                        .value_name("HEADING")
                        .help("Set entry heading.")
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("no-body")
                        .short("n")
                        .long("no-body")
                        .help("Don't promt for body.")
                        .takes_value(false)
                        .required(false)
                        .requires("heading"),
                ),
        )
        .subcommand(
            SubCommand::with_name("edit")
                .about("Edit a journal entry")
                .arg(
                    Arg::with_name("id")
                        .short("i")
                        .long("id")
                        .value_name("ID")
                        .help("Supply entry ID.")
                        .required(true)
                        .takes_value(true),
                ),
        )
        .subcommand(SubCommand::with_name("list").about("Show an entry"))
        .subcommand(
            SubCommand::with_name("read")
                .about("Read a journal entry")
                .arg(
                    Arg::with_name("id")
                        .short("i")
                        .long("id")
                        .value_name("ID")
                        .help("Supply entry ID.")
                        .required(true)
                        .takes_value(true),
                ),
        )
        .subcommand(
            SubCommand::with_name("remove")
                .about("Remove a journal entry")
                .arg(
                    Arg::with_name("id")
                        .short("i")
                        .long("id")
                        .value_name("ID")
                        .help("Supply entry ID.")
                        .required(true)
                        .takes_value(true),
                ),
        )
        .get_matches();

    let opt = match Settings::default() {
        Ok(settings) => settings,
        Err(e) => {
            eprintln!("Failed to load application default settings: {}", e);
            exit(1);
        }
    };

    match args.subcommand_name() {
        Some("add") => sc_add_entry(&opt, &args),
        Some("edit") => sc_edit_entry(&opt, &args),
        Some("list") => sc_list(&opt, &args),
        Some("remove") => sc_remove(&opt, &args),
        Some("read") => sc_read(&opt, &args),
        Some("version") => sc_version(),
        _ => {}
    }
}

fn sc_add_entry(opt: &Settings, args: &ArgMatches) {
    // subcommand add is alrady checked to exist; unwrap is safe.
    let matches = args.subcommand_matches("add").unwrap();

    // Get entry with default values, initialized with heading.
    // Arg parser checks that heading is supplied; unwrap is safe.
    let mut entry = Entry {
        heading: matches.value_of("heading").unwrap_or("").to_string(),
        ..Default::default()
    };

    if !matches.is_present("no-body") {
        let tmp_file = match TmpFile::try_from(&entry) {
            Ok(f) => f,
            Err(e) => {
                eprintln!(
                    "Error: Failed to create edit format tmp file for journal entry: {}",
                    e
                );
                exit(4);
            }
        };

        match edit_tmp_file(&tmp_file) {
            Some(e) => {
                entry = e;
                if let Err(e) = tmp_file.delete() {
                    eprintln!("Error: Failed to delete tmp file: {}", e);
                }
            }
            None => {
                exit(2);
            }
        }
    }

    // save Entry
    match entry.save(opt) {
        Ok(()) => {}
        Err(e) => {
            eprintln!("Failed to save Entry: {}", e.to_string());
        }
    };
}

fn sc_edit_entry(opt: &Settings, args: &ArgMatches) {
    // subcommand edit is alrady checked to exist; unwrap is safe.
    let matches = args.subcommand_matches("edit").unwrap();
    let entry_id = matches.value_of("id").unwrap();

    // get entry for the id
    let entry = match get_entry_by_id_prefix(&opt.data_dir, entry_id) {
        Ok(e) => e,
        Err(e) => {
            eprintln!("Could not get journal entry with id {}: {}", entry_id, e);
            exit(6);
        }
    };

    // so we now have the Entry. How to edit it?
    let mut tmp_file = match TmpFile::try_from(&entry) {
        Ok(tmp) => tmp,
        Err(e) => {
            eprintln!("Failed to get tmp file for editing: {}", e);
            exit(7);
        }
    };

    // If an Entry is returned, the file was edited. If None, it was not
    // edited and nothing needs to be done.
    if edit_tmp_file(&tmp_file).is_some() {
        // Entry was edited, we now have the new entry that needs to replace the old one.
        if let Err(err) = tmp_file.save_as(&entry.path.unwrap()) {
            eprintln!("Error saving the edited journal entry: {}", err);
        };
    }
}

fn sc_list(opt: &Settings, _args: &ArgMatches) {
    let mut entries: Vec<Entry> = get_entries_or_die(&opt.data_dir);

    // sort by date
    entries.sort_by(|a, b| a.date.partial_cmp(&b.date).unwrap());
    let hash_len = minimal_hash_prefix_length(&entries);
    for entry in entries {
        let hash_truncated = entry.sha256().chars().take(hash_len).collect::<String>();
        println!("{} {}: {}", hash_truncated, &entry.date, &entry.heading);
    }
}

fn sc_read(opt: &Settings, args: &ArgMatches) {
    // subcommand edit is alrady checked to exist; unwrap is safe.
    let matches = args.subcommand_matches("read").unwrap();
    let entry_id = matches.value_of("id").unwrap();

    // get entry for the id
    let entry = match get_entry_by_id_prefix(&opt.data_dir, entry_id) {
        Ok(e) => e,
        Err(e) => {
            eprintln!("Could not get journal entry with id {}: {}", entry_id, e);
            exit(6);
        }
    };

    Command::new(get_pager())
        .arg(&entry.path.unwrap())
        .status()
        .expect("Could not launch editor.");
}

/// The 'remove' subcommand. Remove an Entry identified by ID.
fn sc_remove(opt: &Settings, args: &ArgMatches) {
    // subcommand edit is alrady checked to exist; unwrap is safe.
    let matches = args.subcommand_matches("remove").unwrap();
    let entry_id = matches.value_of("id").unwrap();

    // get entry for the id
    let entry = match get_entry_by_id_prefix(&opt.data_dir, entry_id) {
        Ok(e) => e,
        Err(e) => {
            eprintln!("Could not get journal entry with id {}: {}", entry_id, e);
            exit(6);
        }
    };

    if let Err(e) = entry.delete() {
        eprintln!("Failed to delete entry: {}", e);
    }
}

fn sc_version() {
    println!("{} version {}", BIN_NAME, VERSION);
    println!("{}", LICENSE);
}

/// Edit the TmpFile with editor.
///
/// Returns an Entry if the TmpFile was edited, None otherwise.
fn edit_tmp_file(tmp_file: &TmpFile) -> Option<Entry> {
    let mut new_entry = None;
    let mut done = false;

    while !done {
        // launch editor to edit file
        Command::new(get_editor())
            .arg(&tmp_file.path)
            .status()
            .expect("Could not launch editor.");

        // read new entry back in
        match Entry::try_from(tmp_file) {
            Ok(e) => {
                new_entry = Some(e);
                done = true;
            }
            Err(e) => {
                eprintln!(
                    "Error loading edited tmp file '{:?}': {}",
                    &tmp_file.path, e
                );
                if !prompt_retry() {
                    done = true;
                }
            }
        };
    }
    new_entry
}

fn get_entries_or_die(path: &Path) -> Vec<Entry> {
    let entries: Vec<Entry> = match get_entries_from_dir(path) {
        Ok(e) => e,
        Err(e) => {
            eprintln!(
                "Failed to read entries from dir '{}': {}",
                path.to_string_lossy(),
                e.to_string()
            );
            exit(3);
        }
    };

    entries
}

fn get_entry_by_id_prefix(root: &Path, prefix: &str) -> Result<Entry> {
    let mut matches: Vec<Entry> = Vec::new();

    // get all matches for prefix
    let entries = get_entries_or_die(root);
    for entry in entries {
        if entry.sha256_prefix_matches(prefix) {
            matches.push(entry);
        }
    }

    match matches.pop() {
        Some(e) => {
            if matches.is_empty() {
                // ok, single match found
                Ok(e)
            } else {
                // error, multiple matches
                Err(Error::PrefixTooShort)
            }
        }
        None => {
            // error, no matches
            Err(Error::InvalidPrefix)
        }
    }
}

fn get_editor() -> String {
    env::var("EDITOR").unwrap_or_else(|_| DEFAULT_EDITOR.to_string())
}

fn get_pager() -> String {
    env::var("PAGER").unwrap_or_else(|_| DEFAULT_PAGER.to_string())
}

fn prompt_retry() -> bool {
    let mut input = String::new();
    let mut input_ok = false;
    let mut retry = false;

    while !input_ok {
        println!("Edit file or quit without saving (E/q)?");

        input.clear();
        if let Err(e) = std::io::stdin().read_line(&mut input) {
            eprintln!("Error: could not read input from stdin: {}", e);
            eprintln!("Quitting.");
            exit(5);
        };
        input.to_lowercase();

        match input.chars().next() {
            Some(c) => {
                match c {
                    'e' => {
                        input_ok = true;
                        retry = true;
                    }
                    'q' => {
                        input_ok = true;
                        retry = false;
                    }
                    _ => {
                        eprintln!("Can't parse input '{}'.\n", c);
                    }
                };
            }
            None => {
                input_ok = true;
                retry = true;
            }
        };
    }

    retry
}
