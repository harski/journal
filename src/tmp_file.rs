use crate::entry::Entry;
use crate::error;
use crate::path_utils::get_tmp_dir;
use rand::distributions::Alphanumeric;
use rand::{thread_rng, Rng};
use std::convert::TryFrom;
use std::fs::{remove_file, rename, File};
use std::io::{Error, ErrorKind, Result, Write};
use std::path::{Path, PathBuf};

pub const TMP_FILE_BASE: &str = ".tmp.md.";
const TMP_FILE_SUFFIX: &str = ".txt";
const TMP_RANDOM_PART_LEN: u32 = 20;

pub struct TmpFile {
    pub path: PathBuf,
}

impl TmpFile {
    /// Delete the TmpFile.
    pub fn delete(self) -> Result<()> {
        remove_file(&self.path)?;
        Ok(())
    }

    /// Get writeble file handle to the file in path.
    pub fn file_writable(&self) -> Result<File> {
        File::create(self.path.as_path())
    }

    /// Get tmp file path in data_dir.
    ///
    /// The data_dir is checked, and the first non-existent file in the set
    /// pattern will be returned. It is not guaranteed, that the file can be
    /// written to.
    fn generate_tmp_file_path(data_dir: &Path) -> Result<PathBuf> {
        let mut rng = thread_rng().sample_iter(&Alphanumeric);
        let mut path_random = String::new();

        for _ in 0..TMP_RANDOM_PART_LEN {
            let mut path = PathBuf::from(data_dir);
            path_random.push(rng.next().unwrap());

            let mut file_name = String::from(TMP_FILE_BASE);
            file_name.push_str(&path_random);
            file_name.push_str(TMP_FILE_SUFFIX);

            path.push(&file_name);

            if !path.exists() {
                return Ok(path);
            }
        }

        let error_str = format!(
            "Could not get a valid tmp file name. Please clean up old tmp files in directory '{}'.",
            data_dir.display()
        );
        Err(Error::new(ErrorKind::Other, error_str))
    }

    /// Get a TmpFile.
    pub fn new() -> Result<TmpFile> {
        TmpFile::with_dir(get_tmp_dir()?.as_path())
    }

    /// Save a valid TmpFile as Entry.
    pub fn save_as(&mut self, path: &Path) -> Result<()> {
        rename(&self.path, path)?;
        Ok(())
    }

    /// Validate a TmpFile as proper Entry.
    pub fn validate(&self) -> error::Result<()> {
        Entry::try_from(self)?;
        Ok(())
    }

    /// Get a TmpFile in the set dir.
    pub fn with_dir(dir: &Path) -> Result<TmpFile> {
        // get the PathBuf
        let path = TmpFile::generate_tmp_file_path(dir)?;

        // test that it can be written to
        {
            File::create(path.as_path())?;
        }

        // all is well
        Ok(TmpFile { path })
    }

    /// Write a string to TmpFile.path.
    pub fn write_all(&self, content: &str) -> Result<()> {
        let mut file = self.file_writable()?;
        file.write_all(content.as_bytes())?;
        Ok(())
    }
}

impl TryFrom<&Entry> for TmpFile {
    type Error = Error;

    fn try_from(entry: &Entry) -> Result<TmpFile> {
        let tmp_file = TmpFile::new()?;
        if let Err(e) = tmp_file.write_all(&entry.to_string()) {
            return Err(e);
        }
        Ok(tmp_file)
    }
}
