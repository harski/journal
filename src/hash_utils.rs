use crate::entry::Entry;

/// Get common prefix for two string slices.
fn common_prefix_len(s1: &str, s2: &str) -> usize {
    let mut len = 0;

    let mut s1cs = s1.chars();
    let mut s2cs = s2.chars();
    while let (Some(c1), Some(c2)) = (s1cs.next(), s2cs.next()) {
        if c1 == c2 {
            len += 1;
        } else {
            break;
        }
    }

    len
}

/// Calculate minimal hash prefix that uniquely distinquishes all the entries.
pub fn minimal_hash_prefix_length(entries: &[Entry]) -> usize {
    // If we only have 1 (or zero) items, one character suffices.
    if entries.len() < 2 {
        return 1;
    }

    let mut len = 1;
    let mut hashes = Vec::with_capacity(entries.len());

    for entry in entries {
        hashes.push(entry.sha256());
    }

    hashes.sort();
    let mut iter = hashes.iter();
    let mut old = iter.next().unwrap();
    for new in iter {
        let tmp = common_prefix_len(&old, &new);
        if tmp > len {
            len = tmp;
        }
        old = new;
    }

    // The longest common prefix was len, so the shortest to uniquely identify
    // a hash is len + 1
    len + 1
}

#[cfg(test)]
mod tests {
    use super::{common_prefix_len, minimal_hash_prefix_length};
    use crate::entry::Entry;
    use chrono::NaiveDate;

    #[test]
    fn cpf_general() {
        assert_eq!(2, common_prefix_len("abcd", "abdc"));
    }

    #[test]
    fn calc_hash_prefix_same() {
        assert_eq!(4, common_prefix_len("abcd", "abcd"));
    }

    #[test]
    fn calc_hash_prefix_simple() {
        let mut e1 = Entry::default();
        e1.heading = "test".to_string();
        e1.date = NaiveDate::from_ymd(2019, 11, 19);
        let mut e2 = Entry::default();
        e2.heading = "test".to_string();
        e2.date = NaiveDate::from_ymd(2019, 11, 12);

        let mut entries = Vec::new();
        entries.push(e1);
        entries.push(e2);

        let n = minimal_hash_prefix_length(&entries);
        assert!(2 < n);
    }

    #[test]
    fn calc_hash_prefix_too_few() {
        let mut entries = Vec::new();
        assert_eq!(1, minimal_hash_prefix_length(&entries));

        entries.push(Entry::default());
        assert_eq!(1, minimal_hash_prefix_length(&entries));
    }
}
