use std::error;
use std::fmt;

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    ChronoParse(chrono::ParseError),
    InvalidHeading,
    InvalidPrefix,
    Io(std::io::Error),
    PathDoesNotExist,
    PathGeneration,
    PrefixTooShort,
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Error::ChronoParse(ref e) => e.fmt(f),
            Error::InvalidHeading => write!(
                f,
                "Please use a heading consisting of at least one non-whitespace ascii character"
            ),
            Error::InvalidPrefix => write!(f, "The prefix matches multiple entries."),
            Error::Io(ref e) => e.fmt(f),
            Error::PathDoesNotExist => write!(f, "The path does not exist."),
            Error::PathGeneration => write!(f, "Failed to generate a unique path."),
            Error::PrefixTooShort => write!(f, "Entry prefix too short to identify an entry"),
        }
    }
}

impl error::Error for Error {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match *self {
            Error::ChronoParse(ref e) => Some(e),
            Error::InvalidHeading => None,
            Error::InvalidPrefix => None,
            Error::Io(ref e) => Some(e),
            Error::PathDoesNotExist => None,
            Error::PathGeneration => None,
            Error::PrefixTooShort => None,
        }
    }
}

impl From<chrono::ParseError> for Error {
    fn from(err: chrono::ParseError) -> Error {
        Error::ChronoParse(err)
    }
}

impl From<std::io::Error> for Error {
    fn from(err: std::io::Error) -> Error {
        Error::Io(err)
    }
}
