# Journal

Journal is a simple app for keeping a journal, written in Rust.


## Build and Install

```
cargo build --release
sudo ./install.sh
```


## Usage

Examples of usage:

```
# get help
journal --help

# add an entry interactively
journal add

# add an entry without promting for a body
journal add --no-body -h "The heading"

# or with a different date
journal add --no-body -h "The heading" -d "2019-11-30"

# list all entries, the string in the first column is the entry ID
journal list

# edit an entry, look the id up with 'journal list'
journal edit -i ID

# show an entry
journal read -i ID

# remove an entry
journal remove -i ID
```


## Contributing and Bugs

Open up an issue in the project repository in
[GitLab](https://gitlab.com/harski/journal/).


## License

The project is licenced under the 2-clause BSD license. See the LICENSE file in
the project root for details.
