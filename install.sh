#!/bin/sh

bin=journal
target=./target/release/main
path=/usr/local/bin

if [ ! -e "$target" ]; then
	echo "Error: source binary '$target' missing." >&2
	echo "Has the release been build? Try 'cargo build --release'." >&2
fi

cp $target $path/$bin
